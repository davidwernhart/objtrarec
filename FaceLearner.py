import FaceTools as ft
import hypertools as hyp
import numpy as np
from joblib import dump
import os
import imutils
import cv2
from sklearn.neighbors import KNeighborsClassifier

def getImagesDescriptions(rootDir):
    facedata = np.zeros((1, 128))
    facetags = []
    for dirName, subdirList, fileList in os.walk(rootDir):
        for fname in fileList:
            tag = dirName.split('\\')[1]
            image = cv2.imread(os.path.join(dirName, fname))
            image = imutils.resize(image, width=600)
            chips, rects = ft.getFaceChips(image,True)
            for chip in zip(chips):

                facedata= np.append(facedata,ft.getFaceDescriptor(chip),axis=0)
                facetags.append(tag)

                print("tag: " + tag + " src: " + fname)

    facedata = np.delete(facedata, (0), axis=0)
    return facedata, facetags


descriptions,tags = getImagesDescriptions("pictures")
hyp.plot(descriptions, '.', hue=tags, legend=list(set(tags)))

model = KNeighborsClassifier(n_neighbors=3)
model.fit(descriptions,tags)
dump(model,"model.txt")
