from utilities.camera.Camera import Camera
import imutils
import cv2
import FaceTools as ft
from joblib import dump, load
import multiprocessing as mp
import traceback

def camStreamer(imageQueue,boxesQueue):
    try:
        camera = Camera()
        camera.start_capture()
        boxes = None
        chips = None
        tags = None
        while True:
            frame = camera.current_frame.read()
            frame = imutils.resize(frame, width=300)

            writeNormQueue(frame.copy(),imageQueue)

            if(boxesQueue.full()):
                boxes,chips,tags = boxesQueue.get()

            if(boxes is not None):
                for  box,tag,chip in zip(boxes, tags,chips):
                    (x, y, w, h) = box
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255))
                    cv2.putText(frame, str(tag), (x , y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255))

            cv2.imshow("frame",frame)
            cv2.waitKey(10)
    except:
        print ("FATAL: camStreamer exited while multiprocessing")
        traceback.print_exc()

def faceLabeler(imageQueue,boxesQueue):
    try:
        print("faceLabeler started!")
        model = load("model.txt")
        while True:
            #if (imageQueue.full()):
                image = imageQueue.get()
                chips, boxes = ft.getFaceChips(image, False)
                tags = []
                for chip, box in zip(chips, boxes):
                    tag = ft.classifyChip(model, chip)
                    tags.append(tag)
                    #cv2.imshow("chip",image)
                writeNormQueue([boxes,chips,tags],boxesQueue)
                cv2.waitKey(1)


    except:
        print ("FATAL: faceLabeler exited while multiprocessing")
        traceback.print_exc()


def writeNormQueue(var,queue):
    if (queue.full()):
        try:
            queue.get_nowait()
        except:
            print("anormaly with emptying queue!")

    queue.put(var)

if __name__ == '__main__':
    imageQueue = mp.Queue(1)
    boxesQueue = mp.Queue(1)

    streamP = mp.Process(target=camStreamer, args=(imageQueue,boxesQueue,))
    labelP = mp.Process(target=faceLabeler,args=(imageQueue,boxesQueue,))

    streamP.start()
    labelP.start()

