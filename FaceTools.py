import numpy as np
import imutils.face_utils as face_utils
from imutils.face_utils import rect_to_bb
import dlib
import cv2



detector = dlib.get_frontal_face_detector()
facepred = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
facerec = dlib.face_recognition_model_v1("dlib_face_recognition_resnet_model_v1.dat")

facestore = np.zeros((1, 128))
facetags = []


def classifyChip(model,chip):
    descriptor = getFaceDescriptor(chip)
    return model.predict(descriptor)

def annotateImage(model,image):
    chips, boxes = getFaceChips(image,True)
    for chip, box in zip(chips, boxes):
        tag = classifyChip(model,chip)
        (x, y, w, h) = box
        cv2.rectangle(image,(x,y),(x+w,y+h),(0,0,255))
        cv2.putText(image,str(tag),(x,y),cv2.FONT_HERSHEY_SIMPLEX,0.7,(0,0,255))

    return image

def getFaceChips(image, showShape):

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 2)
    chips = []
    boxes = []
    shapes = []
    for rect in rects:
        boxes.append(rect_to_bb(rect))
        shape = facepred(image, rect)
        shapenp = face_utils.shape_to_np(shape)
        shapes.append(shapenp)
        chips.append(dlib.get_face_chip(image, shape))
        if(showShape):
            for (x, y) in shapenp:
                cv2.circle(image, (x, y), 1, (0, 255, 0), -1)

    if(showShape):
        cv2.imshow("annotated",image)
        cv2.waitKey(1)

    return chips,boxes

def getFaceDescriptor(facechip):
    face_descriptor = facerec.compute_face_descriptor(facechip)
    return [np.array(face_descriptor)]


